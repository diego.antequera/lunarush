const { Browser } = require('./src/browser')
const { Metamask } = require('./src/metamask.js')

const defaultPassword = 'os8aduaho641'
const bscNetworkInfo = {
  name: 'Binance Smart Chain Mainnet',
  rpcUrl: 'https://bsc-dataseed.binance.org/',
  chainId: '56',
  currencySymbol: 'BNB',
  blockExplorer: 'https://bscscan.com/',
}
const tokenContractAddress = '0xde301d6a2569aefcfe271b9d98f318baee1d30a4'
const tokenSymbol = 'LUS'
const baseUrl = 'http://app.lunarush.io/'
doRetry = true

class IvoryTower {
  constructor() {
    this.doRetry = true
    this._browser = new Browser()
  }

  get metamask() {
    return this._metamask
  }

  get browser() {
    return this._browser
  }

  get gamePage() {
    return this._gamePage
  }

  async setupWallet() {
    console.log('-- setup wallet')
    // making sure metamask page has loaded
    const metamaskPage = await this.browser.getPage('#initialize')

    console.log('--- Initializing Metamask')
    const metamask = new Metamask(metamaskPage)
    const url = await metamaskPage.url()
    await metamask.initializeMetamask(defaultPassword)
    console.log('Adding network', bscNetworkInfo)
    await metamask.addNetwork(bscNetworkInfo)
    await metamask.importPrivateKey(process.env.ACCOUNT_PRIVATE_KEY)
    await metamask.addToken(tokenSymbol, tokenContractAddress)
    await metamaskPage.goto(url)
    await metamaskPage.waitForTimeout(2000)
    this._metamask = metamask
    console.log('-- done')
  }

  async setupGame() {
    console.log('-- setup game components')
    if (this.gamePage)
      return this.login()

    console.log('--- opening main page')
    this._gamePage = await this.browser.getNewPage(baseUrl)
    console.log('--- connecting account')
    await this.gamePage.waitForTimeout(20000)
    console.log('--- clicking connect wallet button')
    await this.gamePage.waitForImageAndClick('btn_connectWallet')
    await this.gamePage.waitForTimeout(1000)
    console.log('--- searching for a connect window now')
    let connectAccountPopup = await this.browser.getPage('notification.html#connect')
    console.log('--- found it! connecting account')
    await this.metamask.connectAccount(connectAccountPopup)
    await this.gamePage.waitForTimeout(1000)
    console.log('-- done')
  }

  async login() {
    console.log('--- login')
    await this.gamePage.waitForTimeout(10000)
    console.log('--- clicking connect wallet button')
    await this.gamePage.waitForImageAndClick('btn_connectWallet')
    console.log('--- searching for a signin window now')
    let signinPopup = await this.browser.getPage('signature-request')
    console.log('--- found it! signing in')
    await this.metamask.signIn(signinPopup)
    await this.gamePage.waitForTimeout(1000)
    console.log('-- done')
  }

  async close() {
    if (this.browser)
      await this.browser.close()
  }

  async run() {
    try {
      console.log('- run')
      if (!this.metamask)
        await this.setupWallet()

      if (!this.gamePage)
        await this.setupGame()
      await this.login()
      console.log('- all setups done, waiting for it to load')
      await this.gamePage.waitForImage('btn_bossHunt', {
        timeout: 120000
      })

      await this.gamePage.clickOnImage('btn_bossHunt')
      await this.findHighestBoss()

      await this.gamePage.waitForTimeout(1000)
      await this.gamePage.waitForImageAndClick('btn_expandHeroes')
      await this.gamePage.waitForTimeout(1000)

      console.log('- farming')
      await this.farm()

      console.log('- ran! [CODE_COMPLETED_MOVE_TO_NEXT]')
      this.close()
      process.exit()
    }
    catch (e) {
      // reload something goes wrong
      console.log('oops somethings wrong', e)
      if (doRetry) {
        await this.gamePage.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
        doRetry = false
        console.log('error! retrying...', { e })
        return this.run()
      }

      console.log(' fatal error :( ', e.message)
      return this.restart()
    }
  }

  async restart() {
    console.log('restarting all this shit...')
    await this.close()
    getRich()
  }

  async farm() {
    await this.deselectHeroes()
    if (!await this.selectHighestHeroes()) {
      console.log('-- all done here!')
      return true
    }
    console.log('-- starting the fight!')
    await this.gamePage.clickOnImage('btn_startBossHunt', { confidence: 0.9 })
    await this.gamePage.waitForImage('ico_inStage')
    await this.gamePage.clickOnImage('ico_inStage')
    await this.awaitFightEnd()
    await this.fightEnded()
    await this.gamePage.waitForTimeout(4000)
    console.log('-- checking if boss is dead or not')
    if (await this.gamePage.findImage('lbl_bossHunt')) {
      console.log('-- boss is dead, reselecting the highest one')
      await this.findHighestBoss()
    }
    return this.farm()
  }

  async findHighestBoss() {
    console.log('- entering boss hunt')
    await this.gamePage.waitForTimeout(1000)
    console.log('- trying to find active boss')
    if (await this.gamePage.findImage('btn_activeBoss',
      {
        useCache: false
      })) {
      console.log('-- found!!')
      await this.gamePage.clickOnImage('btn_activeBoss')
      return true
    }

    console.log('- checking highest level')
    const levels = [5, 4, 3, 2, 1]
    let bossLevel = false
    let isFound = false
    for (const i in levels) {
      const level = levels[i]
      bossLevel = `btn_bossLevel${level}`
      console.log(`-- checking ${bossLevel}`)
      isFound = await this.gamePage.findImage(bossLevel, { confidence: 0.9 })
      if (isFound) {
        console.log('-- found!!')
        break
      }
    }
    if (!isFound)
      throw new Error('no boss available')
    await this.gamePage.clickOnImage(bossLevel)
  }

  async deselectHeroes() {
    console.log('-- deselecting heroes')
    if (await this.gamePage.findImage('btn_expandHeroes', { useCache: false })) {
      console.log('-- expanding the heroes list')
      await this.gamePage.clickOnImage('btn_expandHeroes')
    }

    if (!await this.gamePage.findImage('btn_heroSelected',
      {
        useCache: false,
        confidence: 0.97
      })) {
      console.log('-- done!')
      return true
    }
    console.log('-- deselected')
    await this.gamePage.clickOnImage('btn_heroSelected')
    await this.gamePage.waitForTimeout(3000)
    return this.deselectHeroes()
  }

  async selectHighestHeroes(heroesSelected = 0, currentLevel = 3) {
    console.log(`-- selecting highest heroes; ${heroesSelected}/3; looking for level ${currentLevel}`)
    if (heroesSelected >= 3 || (currentLevel <= 0 && heroesSelected > 0)) {
      console.log('-- already selected all')
      return true
    }
    if (currentLevel <= 0) {
      console.log('-- no more heroes available :(')
      return false
    }
    const heroLevel = `btn_heroEnergy${currentLevel}`
    if (!await this.gamePage.findImage(heroLevel,
      {
        useCache: false,
        confidence: 0.96
      })) {
      console.log('-- no heroes available on this level, lowering')
      return this.selectHighestHeroes(heroesSelected, currentLevel - 1)
    }
    console.log('-- found! selecting!')
    await this.gamePage.clickOnImage(heroLevel)
    await this.gamePage.waitForTimeout(3000)
    return this.selectHighestHeroes(heroesSelected + 1, currentLevel)
  }

  async awaitFightEnd() {
    console.log('--- checking if fight ended...')
    try {
      await this.gamePage.clickOnImage('ico_inStage', {
        useCache: false
      })
      await this.gamePage.waitForTimeout(2000)
    }
    catch (e) {
      return true
    }
    return this.awaitFightEnd()
  }

  async fightEnded() {
    console.log('--- fight ended! checking results')
    await this.gamePage.waitForTimeout(1000)
    if (await this.gamePage.findImage('btn_tapToOpen')) {
      console.log('-- victory! collecting the spoils')
      await this.gamePage.clickOnImage('btn_tapToOpen')
      await this.gamePage.waitForTimeout(4000)
      await this.gamePage.clickOnImage('lbl_victory')
    }
    else {
      console.log('-- defeat :( returning to previous screen')
      await this.gamePage.clickOnImage('lbl_defeat')
    }
  }
}

async function getRich() {
  const tower = new IvoryTower()
  try {
    await tower.run()
  }
  catch (e) {
    console.log('error!', { e })
    await tower.close()
    return getRich()
  }
}

getRich()
// console.log(`it should be running this ${process.env.BCOIN_ACCOUNT}`)
