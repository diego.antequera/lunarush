#!/bin/bash

private_keys=($(awk -F= '{print $1}' .keys))
while [ 1=1 ]
do
  echo "\n\n======================= starting all over"
  for private_key in "${private_keys[@]}"
  do
    export ACCOUNT_PRIVATE_KEY=$private_key
    export BOT_WINDOW_LEFT=0
    export BOT_WINDOW_TOP=0
    export BOT_WINDOW_WIDTH=1200
    export BOT_WINDOW_HEIGHT=800
    echo "\n\n\n======================================================"
    echo "RUNNING FOR PRIVATE_KEY STARTING IN ${private_key:0:8}"
    node main.js
    sleep 5
  done
  sleep_for=$((( RANDOM % 720 ) + 720))
  now=$(date)
  then=$(date -v+${sleep_for}S)
  echo "[$now] sleeping for $sleep_for seconds. check back at ${then}"
  sleep $sleep_for
done