const puppeteer = require("puppeteer-extra")
const StealthPlugin = require("puppeteer-extra-plugin-stealth")
const { expandPage } = require("./page.js")

const metamaskPath = `${process.cwd()}/extensions/metamask`

class Browser {
  async getBrowser() {
    if (!this._browser) {
      this._browser = await puppeteer
        .use(StealthPlugin())
        .launch({
          headless: false,
          args: [
            '--mute-audio',
            `--load-extension=${metamaskPath}`,
            `--disable-extensions-except=${metamaskPath}`,
            '--no-sandbox',
            '--disable-setuid-sandbox',
            // This will write shared memory files into /tmp instead of /dev/shm,
            // because Docker’s default for /dev/shm is 64MB
            '--disable-dev-shm-usage',
            `--window-size=${process.env.BOT_WINDOW_WIDTH || 1200},${process.env.BOT_WINDOW_HEIGHT || 800}`,
            `--window-position=${process.env.BOT_WINDOW_LEFT || 0},${process.env.BOT_WINDOW_TOP || 0}`
          ]
        })
      // warm up page objects
      const page = await this._browser.newPage()
      expandPage(page)
      await page.close()
    }
    return this._browser
  }

  async getNewPage(url) {
    // check if theres an empty page first
    const browser = await this.getBrowser()
    const page = await browser.newPage()

    if (url) {
      await page.goto(url, { waitUntil: 'networkidle2' });
    }
    // adds custom convenience methods to page object
    expandPage(page)
    return page
  }

  async getOpenPages() {
    let browser = await this.getBrowser()
    return await browser.pages()
  }

  async getPage(urlPiece, timeout = 30000) {
    let browser = await this.getBrowser()
    let pageTarget = await browser.waitForTarget(
      (p) => p.url().includes(urlPiece),
      { timeout: timeout })
    return await pageTarget.page()
  }

  async close() {
    let browser = await this.getBrowser()
    await browser.close()
  }
}

module.exports = { Browser }
