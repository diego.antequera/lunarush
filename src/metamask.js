const replaceKeyword = '__REPLACE__'

const pageSelectors = {
  buttons: {
    home: 'img.app-header__metafox-logo',
    startNow: 'button.first-time-flow__button',
    createWallet: 'div.select-action__select-buttons > div:nth-child(2) > button',
    noThanks: 'button.btn-default',
    createPassword: 'button.first-time-flow__button',
    next: 'button.btn-primary',
    remindMeLater: 'button.btn-secondary',
    closePopover: 'button[title=Close]',
    addNetwork: 'button.btn-secondary',
    saveNewNetwork: 'div.network-form__footer > button.btn-secondary',
    importAccount: 'img[src*=import-account]',
    importPrivateKey: 'div.new-account-import-form button.btn-secondary',
    connectAccountNext: 'button.btn-primary',
    connectAccountConfirm: 'button.btn-primary',
    signIn: 'button.btn-secondary',
    confirmTransaction: 'button.btn-primary',
    swap: 'button.icon-button:nth-child(3)',
    twoPercentSlippage: 'div.slippage-buttons__button-group > button',
    reviewSwap: 'button.btn-primary',
    confirmSwap: 'button.btn-primary',
    closeSwap: 'button.btn-primary',
    addToken: 'button.btn-secondary',
    addTokenNext: 'button.btn-secondary',
    approveTransaction: 'button.btn-primary'
  },
  inputs: {
    newPassword: 'input#create-password',
    confirmPassword: 'input#confirm-password',
    agreeTos: 'div.first-time-flow__checkbox',
    privateKey: 'input#private-key-box',
    newNetwork: {
      name: 'input#network-name',
      rpcUrl: 'input#rpc-url',
      chainId: 'input#chainId',
      currencySymbol: 'input#network-ticker',
      blockExplorer: 'input#block-explorer-url',
    },
    connectAccount: {
      selectAll: 'div.permissions-connect-choose-account__select-all > input.check-box',
      selectAllChecked: 'div.permissions-connect-choose-account__select-all > input.check-box__checked',
      selectAllUnchecked: 'div.permissions-connect-choose-account__select-all > input.fa-square'
    },
    convertTokenSearch: 'div.dropdown-search-list--open input',
    convertTokenToSearch: 'div.dropdown-input-pair__to input',
    convertTokenValue: 'div.dropdown-input-pair input',
    tokenAddress: 'input#custom-address',

  },
  icons: {
    account: 'div.account-menu__icon',
    settings: 'img[src*=settings]'
  },
  labels: {
    seedPhrase: 'div.seed-phrase-intro__right',
    walletAddress: 'div.selected-account__address',
    connectAccountAddresses: 'div.permissions-connect-choose-account__account__info',
    connectAccountXPath: `//div[contains(text(), '${replaceKeyword}')]`,
    transactionFee: 'div.transaction-detail-item div.currency-display-component',
    swapProgress: 'div.awaiting-swap__header',
    tokenBalance: `div.tabs__content button[title*=${replaceKeyword}]`
  },
  dropdowns: {
    currencySwapFrom: 'div.dropdown-search-list',
    currencySwapTo: 'div.dropdown-input-pair__to > div',
    currencySearchXPath: `//div[contains(@class, 'build-quote__open-dropdown')]//span[contains(text(), '${replaceKeyword}')]`,
    currencySearchToXPath: `//div[contains(@class, 'build-quote__open-to-dropdown')]//span[contains(text(), '${replaceKeyword}')]`,
    bnbToken: 'img[src*=bnb]'
  }
}

class Metamask {
  constructor(page) {
    this._page = page
  }

  get page() {
    return this._page
  }

  get walletAddress() {
    return this._walletAddress
  }

  async initializeMetamask(defaultPassword) {
    const page = this.page
    await page.bringToFront()
    await page.waitForTimeout(1000)
    await page.waitAndClick(pageSelectors.buttons.startNow)
    await page.waitAndClick(pageSelectors.buttons.createWallet)
    await page.waitAndClick(pageSelectors.buttons.noThanks)
    await page.waitForSelector(pageSelectors.inputs.newPassword)
    await page.typeText(pageSelectors.inputs.newPassword, defaultPassword)
    await page.typeText(pageSelectors.inputs.confirmPassword, defaultPassword)
    await page.click(pageSelectors.inputs.agreeTos)
    await page.click(pageSelectors.buttons.createPassword)
    await page.waitForSelector(pageSelectors.labels.seedPhrase)
    await page.waitForTimeout(2000)
    await page.clickUntilSelector(pageSelectors.buttons.next, pageSelectors.buttons.remindMeLater)
    await page.waitAndClick(pageSelectors.buttons.remindMeLater)
    await page.waitForTimeout(2000)
    await page.waitAndClick(pageSelectors.icons.account)

  }

  async addNetwork(networkInfo) {
    const page = this.page
    const baseUrl = await page.url()
    await page.bringToFront()
    await page.goto(`${baseUrl}settings/networks`)
    await page.waitAndClick(pageSelectors.buttons.addNetwork)

    await page.waitForSelector(pageSelectors.inputs.newNetwork.name)
    for (var i in pageSelectors.inputs.newNetwork) {
      await page.typeText(pageSelectors.inputs.newNetwork[i], networkInfo[i])
    }
    await page.click(pageSelectors.buttons.saveNewNetwork)
    await page.goto(`${baseUrl}`)
  }

  async importPrivateKey(privateKey) {
    const page = this.page
    const baseUrl = await page.url()
    await page.bringToFront()
    await page.goto(`${baseUrl}new-account/import`)
    await page.waitForSelector(pageSelectors.inputs.privateKey)
    await page.typeText(pageSelectors.inputs.privateKey, privateKey)
    await page.waitForTimeout(2000)
    await page.waitAndClick(pageSelectors.buttons.importPrivateKey)
    await page.waitForTimeout(2000)
    this._walletAddress = await page.$eval(pageSelectors.labels.walletAddress, e => e.textContent)
    console.log('masked wallet address is ', this._walletAddress)
  }

  async addToken(token, contract) {
    const page = this.page
    const baseUrl = await page.url()
    await page.bringToFront()
    await page.click(pageSelectors.buttons.addToken)
    await page.typeText(pageSelectors.inputs.tokenAddress, contract)
    await page.waitForTimeout(4000)
    await page.focus(pageSelectors.buttons.addTokenNext)
    await page.click(pageSelectors.buttons.addTokenNext)
    await page.click(pageSelectors.buttons.addTokenNext)

    await page.waitForTimeout(1000)
    await page.click(pageSelectors.buttons.home)
  }

  async connectAccount(popupPage) {
    // confirm the right account is checked
    await popupPage.bringToFront()
    await popupPage.clickUntilSelector(
      pageSelectors.inputs.connectAccount.selectAll,
      pageSelectors.inputs.connectAccount.selectAllUnchecked)
    let last4AccountDigits = this.walletAddress.substr(-4).toLowerCase()
    let selector = pageSelectors.labels.connectAccountXPath.replace(replaceKeyword, last4AccountDigits)
    const [account] = await popupPage.$x(selector)
    await account.click()
    await popupPage.waitAndClick(pageSelectors.buttons.connectAccountNext)
    await popupPage.waitForTimeout(1000)
    await popupPage.waitAndClick(pageSelectors.buttons.connectAccountConfirm)
  }

  async signIn(popupPage) {
    await popupPage.bringToFront()
    await popupPage.waitForSelector(pageSelectors.buttons.signIn)
    await popupPage.waitForTimeout(2000)
    await popupPage.waitAndClick(pageSelectors.buttons.signIn)
    await popupPage.waitForTimeout(2000)
  }

  async getTokenBalance(token) {
    const page = this.page
    let tokenSelector = pageSelectors.labels.tokenBalance.replace(replaceKeyword, token)
    console.log('checking balance for ', token)
    try {
      return parseFloat(await page.$eval(tokenSelector, (e) => e.getAttribute('title').slice(0, -4)))
    }
    catch (e) {
      console.log('couldnt find the token, so balance is 0')
      return 0.0
    }
  }

  async swapTokens(amount, tokenFrom, tokenTo) {
    const page = this.page
    const baseUrl = await page.url()
    await page.bringToFront()
    await page.goto(`${baseUrl}#swaps/build-quote`)
    await page.waitForTimeout(2000)

    // select first token
    await page.waitAndClick(pageSelectors.dropdowns.currencySwapFrom)
    await page.waitForTimeout(500)
    await page.typeText(pageSelectors.inputs.convertTokenSearch, tokenFrom, 50)
    let fromTokenXPathSelector = pageSelectors.dropdowns.currencySearchXPath.replace(replaceKeyword, tokenFrom)
    await page.waitForTimeout(1000)
    const [fromToken] = await page.$x(fromTokenXPathSelector)
    await fromToken.click()
    // input amount
    await page.waitForTimeout(1000)
    await page.typeText(pageSelectors.inputs.convertTokenValue, amount.toString())
    // select second token
    await page.click(pageSelectors.dropdowns.currencySwapTo)
    await page.waitForTimeout(500)
    await page.typeText(pageSelectors.inputs.convertTokenToSearch, tokenTo, 50)
    let toTokenXPathSelector = pageSelectors.dropdowns.currencySearchToXPath.replace(replaceKeyword, tokenTo)
    await page.waitForTimeout(1000)
    const [toToken] = await page.$x(toTokenXPathSelector)
    await toToken.click()

    await page.click(pageSelectors.buttons.twoPercentSlippage)
    await page.click(pageSelectors.buttons.reviewSwap)
    await page.waitForTimeout(500000)
    return this.confirmSwap()
  }

  async confirmSwap() {
    const page = this.page
    console.log('checking if quote is done...')
    let url = await page.url()
    if (url.endsWith('view-quote')) {
      console.log('quote is done, confirming')
      await page.click(pageSelectors.buttons.confirmSwap)
      await page.waitForTimeout(5000)
      return this.awaitingSwap()
    }
    console.log('still quoting, will comeback in a sec')
    await page.waitForTimeout(1000)
    return this.confirmSwap()
  }

  async awaitingSwap() {
    const page = this.page
    console.log('awaiting for swap to be completed....')
    let swapStatus = await page.innerText(pageSelectors.labels.swapProgress)
    if (swapStatus.toLowerCase().includes('processing')) {
      console.log('swap processing')
      await page.waitForTimeout(2000)
      return this.awaitingSwap()
    }
    console.log('swap done!')
    await page.click(pageSelectors.buttons.closeSwap)
  }

  async confirmTransaction(popupPage) {
    await popupPage.bringToFront()
    // if (!await popupPage.exists(pageSelectors.labels.transactionFee)) {
    //   await this.signIn(popupPage)
    // }
    // await popupPage.waitForSelector(pageSelectors.labels.transactionFee)
    // let transactionFee = await popupPage.innerText(pageSelectors.labels.transactionFee)
    // console.log(`approving transaction with a fee of ${transactionFee} BNB`)
    await popupPage.click(pageSelectors.buttons.confirmTransaction)
  }

  async approveTransaction(popupPage) {
    await popupPage.bringToFront()
    await popupPage.waitAndClick(pageSelectors.buttons.approveTransaction)
  }
}

module.exports = { Metamask }
